import entity.City;
import method.*;

import java.io.*;
import java.util.*;

public class Solution {

    private void solve(BufferedReader bufferedReader, BufferedWriter bufferedWriter) throws IOException {

        //reading start point and destination
        Map.Entry<String, String> path = initPath(bufferedReader);
        //initialization data
        HashMap<String, City> cities = initMapData(bufferedReader);

        DepthFirstSearchMethod dfsMethod = new DepthFirstSearchMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        dfsMethod.doTask();
        System.out.println("DFS result: " + dfsMethod.showPath() + " Число обойдённых вершин:" + dfsMethod.getCounter());

        BreadthFirstSearchMethod bfsMethod = new BreadthFirstSearchMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        bfsMethod.doTask();
        System.out.println("BFS result: " + bfsMethod.showPath() + " Число обойдённых вершин:" + bfsMethod.getCounter());

        DepthLimitedSearchMethod dlsMethod = new DepthLimitedSearchMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        int depthForDLSMethod = 2;
        dlsMethod.setDepth(depthForDLSMethod + 1);
        dlsMethod.doTask();
        System.out.println("LimS result: " + dlsMethod.showPath() + " Число обойдённых вершин:" + dlsMethod.getCounter());

        IterativeDeepeningDepthFirstSearchMethod iddfsMethod = new IterativeDeepeningDepthFirstSearchMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        iddfsMethod.doTask();
        System.out.println("IDDFS result: " + iddfsMethod.showPath() + " Число обойдённых вершин:" + iddfsMethod.getCounter());

        BiDirectionSearchMethod biDirectionSearchMethod = new BiDirectionSearchMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        biDirectionSearchMethod.doTask();
        System.out.println("BDS result: " + biDirectionSearchMethod.showPath() + " Число обойдённых вершин:" + biDirectionSearchMethod.getCounter());

        GreedySearchFirstMethod gsf = new GreedySearchFirstMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        gsf.doTask();
        System.out.println("GSF result: " + gsf.showPath() + " Итоговое расстояние равно: " + gsf.getSum() + " Число обойдённых вершин: " + gsf.getCounter());


        MinimizingTheTotalScoreAStarMethod aStar = new MinimizingTheTotalScoreAStarMethod(cities, cities.get(path.getKey()), cities.get(path.getValue()));
        aStar.doTask();
        System.out.println("A* result:" + aStar.showPath() + " Итоговое расстояние равно: " + aStar.getSum() + " Число обойдённых вершин: " + aStar.getCounter());


    }

    private HashMap<String, City> initMapData(BufferedReader bufferedReader) throws IOException {
        HashMap<String, City> cities = new HashMap<>();

        while (bufferedReader.ready()) {
            // Reading data from input.txt
            String[] temp = bufferedReader.readLine().split(" ");
            String firstCityName = temp[0];
            String secondCityName = temp[1];
            int distanceBetween = Integer.parseInt(temp[2]);

            var firstCity = cities.containsKey(firstCityName) ? cities.get(firstCityName) : new City(firstCityName);
            var secondCity = cities.containsKey(secondCityName) ? cities.get(secondCityName) : new City(secondCityName);

            firstCity.addDistanceToNeighbourCities(secondCity, distanceBetween);
            secondCity.addDistanceToNeighbourCities(firstCity, distanceBetween);

            cities.put(firstCityName, firstCity);
            cities.put(secondCityName, secondCity);

        }
        return cities;
    }

    private Map.Entry<String, String> initPath(BufferedReader bufferedReader) throws IOException {
        String[] needToCheckDistance = bufferedReader.readLine().split(" ");
        return Map.entry(needToCheckDistance[0], needToCheckDistance[1]);
    }

    private void run() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));
             BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"))) {
            solve(bufferedReader, bufferedWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Solution().run();
    }
}
