package method;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import entity.City;
import entity.Status;

public class DepthFirstSearchMethod implements MethodInterface, ShowPathInterface {

    private final City fromCity;
    private final City destCity;
    private final HashMap<String, City> cities;
    private final StringBuilder stringBuilder;
    public final Stack<Map.Entry<City, City>> edges;
    private int NODE_COUNTER = 0;

    public DepthFirstSearchMethod(HashMap<String, City> cities, City city, City destCity) {
        this.cities = cities;
        this.fromCity = city;
        this.destCity = destCity;
        this.stringBuilder = new StringBuilder();
        this.edges = new Stack<>();
    }

    @Override
    public void doTask() {
        dfs();
    }

    @Override
    public String showPath() {
        return stringBuilder.toString();
    }

    /**
     * If cityStatus's value equals WHITE - it's mean that our city didn't handle yet
     * If cityStatus's value equals GRAY - it's mean that our city are handling right now
     * If cityStatus's value equals BLACK - it's mean that our city proceseed.
     */

    private void dfs() {
        HashMap<City, Status> isViewedCity = new HashMap<>();
        cities.forEach((k, v) -> isViewedCity.put(v, Status.WHITE));

        Stack<City> cityStack = new Stack<>();
        cityStack.push(fromCity);
        isViewedCity.put(fromCity, Status.GRAY);

        NODE_COUNTER++;

        while (!cityStack.isEmpty()) {
            City top = cityStack.pop();
            if (top.equals(destCity)) break;
            isViewedCity.put(top, Status.BLACK);

            NODE_COUNTER++;

            for (Map.Entry<City, Integer> entry : top.getDistanceToNeighbourCities().entrySet()) {
                if (isViewedCity.get(entry.getKey()).equals(Status.WHITE)) {
                    edges.push(Map.entry(top, entry.getKey()));
                    cityStack.push(entry.getKey());
                    isViewedCity.put(entry.getKey(), Status.GRAY);
                }
            }
        }

        getRightOrder();
    }

    private void getRightOrder() {
        City req = destCity;
        stringBuilder.append(destCity).append(" <=== ");
        while (!edges.isEmpty()) {
            Map.Entry<City, City> top = edges.pop();
            if (top.getValue().equals(req)) {
                req = top.getKey();
                if (!req.equals(fromCity)) {
                    stringBuilder.append(req).append(" <=== ");
                } else {
                    stringBuilder.append(req);
                    break;
                }
            }
        }
    }

    public int getCounter() {
        return NODE_COUNTER;
    }
}
