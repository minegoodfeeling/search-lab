package method;

import entity.City;

import java.util.*;

public class DepthLimitedSearchMethod implements MethodInterface, ShowPathInterface {

    private final HashMap<String, City> cities;
    private final City fromCity;
    private final City destCity;
    private final StringBuilder stringBuilder;
    private int depth;
    private int NODE_COUNTER = 0;

    public DepthLimitedSearchMethod(HashMap<String, City> cities, City fromCity, City destCity) {
        this.cities = cities;
        this.fromCity = fromCity;
        this.destCity = destCity;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public void doTask() {
        ArrayList<City> straight;
        try {
            straight = ls(fromCity, destCity);
            straight.forEach(k -> stringBuilder.append(k.getNameOfCity()).append(" ===> "));
            stringBuilder.append(destCity).append(". End of route!");
        } catch (Exception e) {
            stringBuilder.append("На данной глубине(").append(depth - 1).append(")").append(" город ").append(destCity).append(" не был найден");
        }
    }

    @Override
    public String showPath() {
        return stringBuilder.toString();
    }

    public ArrayList<City> ls(City currentCity, City destCity) throws Exception {
        HashMap<City, Integer> countDistance = new HashMap<>();
        ArrayList<City> path = new ArrayList<>();
        Stack<Map.Entry<City, City>> edgesStack = new Stack<>();

        countDistance.put(currentCity, 0);

        Queue<City> queue = new ArrayDeque<>();
        queue.add(currentCity);

        while (!queue.isEmpty()) {
            City top = queue.poll();
            if (top.equals(destCity)) {
                break;
            }
            NODE_COUNTER++;

            int d = countDistance.get(top) + 1;

            if (depth < d) throw new Exception();

            cities.get(top.getNameOfCity()).getDistanceToNeighbourCities().forEach((k, v) -> {
                if (!countDistance.containsKey(k)) {
                    countDistance.put(k, d);
                    edgesStack.add(Map.entry(top, k));
                    queue.add(k);
                }
            });
        }
        getRightPath(edgesStack, path);

        return path;
    }

    public void getRightPath(Stack<Map.Entry<City, City>> edgesStack, ArrayList<City> path) {
        City req = destCity;
        while (!edgesStack.isEmpty()) {
            Map.Entry<City, City> top = edgesStack.pop();

            if (top.getValue().equals(req)) {
                req = top.getKey();
                path.add(req);
            }
        }
        Collections.reverse(path);
    }

    public void resetStringBuilder() {
        stringBuilder.setLength(0);
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getCounter() {
        return NODE_COUNTER;
    }

}
