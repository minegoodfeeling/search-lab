package method;

import entity.City;
import entity.Status;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class GreedySearchFirstMethod implements MethodInterface, ShowPathInterface {
    private final HashMap<String, City> cities;
    private final City fromCity;
    private final City destCity;
    private final StringBuilder stringBuilder;
    private final Stack<Map.Entry<City, City>> edges;
    private int sum = 0;
    private int NODE_COUNTER = 0;

    public GreedySearchFirstMethod(HashMap<String, City> cities, City fromCity, City destCity) {
        this.cities = cities;
        this.fromCity = fromCity;
        this.destCity = destCity;
        this.edges = new Stack<>();
        this.stringBuilder = new StringBuilder();
    }

    private void gsf() {
        HashMap<City, Status> isViewedCity = new HashMap<>();
        cities.forEach((k, v) -> isViewedCity.put(v, Status.WHITE));

        Stack<City> cityStack = new Stack<>();
        cityStack.push(fromCity);
        isViewedCity.put(fromCity, Status.GRAY);

        NODE_COUNTER++;

        while (!cityStack.isEmpty()) {
            int minValue = Integer.MAX_VALUE;
            City top = cityStack.pop();
            if (top.equals(destCity)) break;
            isViewedCity.put(top, Status.BLACK);

            NODE_COUNTER++;

            for (Map.Entry<City, Integer> entry : top.getDistanceToNeighbourCities().entrySet()) {
                if (isViewedCity.get(entry.getKey()).equals(Status.WHITE) && entry.getValue() < minValue) {
                    minValue = entry.getValue();
                    edges.push(Map.entry(top, entry.getKey()));
                    cityStack.push(entry.getKey());
                    isViewedCity.put(entry.getKey(), Status.GRAY);
                }
            }
        }

        getRightOrder();
    }

    private void getRightOrder() {
        City req = destCity;
        stringBuilder.append(destCity).append(" <=(");
        while (!edges.isEmpty()) {
            Map.Entry<City, City> top = edges.pop();
            if (top.getValue().equals(req)) {
                stringBuilder.append(top.getKey().getDistanceToNeighbourCities().get(req)).append(")== ");
                sum += req.getDistanceToNeighbourCities().get(top.getKey());
                req = top.getKey();
                if (!req.equals(fromCity)) {
                    stringBuilder.append(req).append(" <=(");
                } else {
                    stringBuilder.append(req);
                    break;
                }
            }
        }
    }

    public int getSum() {
        return sum;
    }

    public int getCounter() {
        return NODE_COUNTER - 1;
    }

    @Override
    public void doTask() {
        gsf();
    }

    @Override
    public String showPath() {
        return stringBuilder.append(". End of route!").toString();
    }
}
