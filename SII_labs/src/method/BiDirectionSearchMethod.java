package method;

import entity.City;

import java.util.*;

public class BiDirectionSearchMethod implements MethodInterface, ShowPathInterface {
    private final City startCity;
    private final City destCity;
    private final HashMap<String, City> cities;
    private final StringBuilder stringBuilder = new StringBuilder();
    private final Stack<Map.Entry<City, City>> edgesStart;
    private final Stack<Map.Entry<City, City>> edgesFinish;
    private int NODE_COUNTER = 0;

    public BiDirectionSearchMethod(HashMap<String, City> cities, City city, City dest) {
        this.cities = cities;
        this.startCity = city;
        this.destCity = dest;
        this.edgesStart = new Stack<>();
        this.edgesFinish = new Stack<>();
    }

    private void bds() {
        HashMap<String, Integer> isVisitedCityS = new HashMap<>();
        HashMap<String, Integer> isVisitedCityD = new HashMap<>();
        cities.forEach((k, v) -> isVisitedCityS.put(k, 0));
        cities.forEach((k, v) -> isVisitedCityD.put(k, 0));

        Queue<City> queueStart = new ArrayDeque<>();
        Queue<City> queueFinish = new ArrayDeque<>();
        queueStart.add(startCity);
        queueFinish.add(destCity);
        isVisitedCityS.put(startCity.getNameOfCity(), 1);
        isVisitedCityS.put(destCity.getNameOfCity(), 1);

        while (!queueStart.isEmpty()) {
            boolean flag = false;
            City currentCity = queueStart.poll();
            City destCity = queueFinish.poll();
            assert currentCity != null;
            if (currentCity.equals(destCity)) break;


            if (isVisitedCityS.get(currentCity.getNameOfCity()) != 1) {
                continue;
            }
            isVisitedCityS.put(currentCity.getNameOfCity(), 2);

            if (currentCity.getDistanceToNeighbourCities().containsKey(destCity)) break;

            for (Map.Entry<City, Integer> entry : currentCity.getDistanceToNeighbourCities().entrySet()) {
                if (isVisitedCityS.get(entry.getKey().getNameOfCity()) == 0) {
                    queueStart.add(entry.getKey());
                    isVisitedCityS.put(entry.getKey().getNameOfCity(), 1);

                    edgesStart.push(Map.entry(currentCity, entry.getKey()));
                }
                if (isVisitedCityD.get(entry.getKey().getNameOfCity()) == 1) {
                    flag = true;
                    break;
                }

                NODE_COUNTER++;
            }
            if (flag) break;

            assert destCity != null;
            for (Map.Entry<City, Integer> entry : destCity.getDistanceToNeighbourCities().entrySet()) {
                if (isVisitedCityD.get(entry.getKey().getNameOfCity()) == 0) {
                    queueFinish.add(entry.getKey());
                    isVisitedCityD.put(entry.getKey().getNameOfCity(), 1);
                    edgesFinish.push(Map.entry(destCity, entry.getKey()));
                }
                if (isVisitedCityS.get(entry.getKey().getNameOfCity()) == 1) {
                    flag = true;
                    break;
                }

                NODE_COUNTER++;
            }
            if(flag) break;


        }
    }

    private void buildPath() {
        while (!edgesFinish.isEmpty()) {
            Map.Entry<City, City> e = edgesFinish.pop();
            edgesStart.push(Map.entry(e.getKey(), e.getValue()));
        }
        ArrayList<String> out = new ArrayList<>();
        String req = destCity.getNameOfCity();
        out.add(req);
        while (!edgesStart.isEmpty()) {
            Map.Entry<City, City> e = edgesStart.pop();
            if (Objects.equals(e.getValue(), cities.get(req))) {
                req = e.getKey().getNameOfCity();
                out.add(req);
            }
        }
        Collections.reverse(out);
        for(String s: out) {
            stringBuilder.append(s).append(" ==> ");
        }
    }

    @Override
    public String showPath() {
        return stringBuilder.append(". End of route!").toString();
    }

    @Override
    public void doTask() {
        bds();
        buildPath();
    }

    public int getCounter() {
        return NODE_COUNTER;
    }
}
