package method;

import entity.City;

import java.util.HashMap;

public class IterativeDeepeningDepthFirstSearchMethod implements MethodInterface, ShowPathInterface {
    private final HashMap<String, City> cities;
    private final City fromCity;
    private final City toCity;
    private final StringBuilder stringBuilder;
    private int NODE_COUNTER;


    public IterativeDeepeningDepthFirstSearchMethod(HashMap<String, City> cities, City fromCity, City toCity) {
        this.cities = cities;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public void doTask() {
        DepthLimitedSearchMethod dlsMethod = new DepthLimitedSearchMethod(cities, fromCity, toCity);
        dlsMethod.doTask();
        int iterLimit = 0;
        dlsMethod.setDepth(iterLimit);
        NODE_COUNTER = dlsMethod.getCounter();

        while (!dlsMethod.showPath().contains("End of route!")) {
            if (dlsMethod.getCounter() > NODE_COUNTER) NODE_COUNTER = dlsMethod.getCounter();
            dlsMethod.resetStringBuilder();
            dlsMethod.doTask();
            iterLimit++;
            dlsMethod.setDepth(iterLimit);
        }

        stringBuilder.append(dlsMethod.showPath()).append(" ");
    }

    @Override
    public String showPath() {
        return stringBuilder.toString();
    }

    public int getCounter() {
        return NODE_COUNTER;
    }
}
