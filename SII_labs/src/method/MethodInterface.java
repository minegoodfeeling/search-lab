package method;

public interface MethodInterface {
    void doTask() throws Exception;
}
