package method;

import entity.City;
import entity.Status;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class MinimizingTheTotalScoreAStarMethod implements MethodInterface, ShowPathInterface{

    private final HashMap<String, City> cities;
    private final City fromCity;
    private final City destCity;
    private final StringBuilder stringBuilder;
    private final HashMap<City, Integer> directDistancesToNN;
    private final Stack<Map.Entry<City, City>> edges;
    private int NODE_COUNTER = 0;

    private int sum = 0;

    public MinimizingTheTotalScoreAStarMethod(HashMap<String, City> cities, City fromCity, City destCity) {
        this.cities = cities;
        this.fromCity = fromCity;
        this.destCity = destCity;
        this.stringBuilder = new StringBuilder();
        this.directDistancesToNN = new HashMap<>();
        this.edges = new Stack<>();
    }


    @Override
    public void doTask(){
        initDirectDistances();
        aStar();
    }

    private void initDirectDistances() {
        directDistancesToNN.put(cities.get("Kharkiv"), 868);
        directDistancesToNN.put(cities.get("Kiev"), 1105);
        directDistancesToNN.put(cities.get("Zhitomir"), 1218 );
        directDistancesToNN.put(cities.get("Donetsk"), 932);
        directDistancesToNN.put(cities.get("Orel"), 632);
        directDistancesToNN.put(cities.get("Moscow"), 402);
        directDistancesToNN.put(cities.get("Vilnius"), 1189);
        directDistancesToNN.put(cities.get("Kaliningrad"), 1487);
        directDistancesToNN.put(cities.get("Vitebsk"), 868);
        directDistancesToNN.put(cities.get("Voronezh"), 607);
        directDistancesToNN.put(cities.get("Brest"), 1390);
        directDistancesToNN.put(cities.get("Volgograd"), 848);
        directDistancesToNN.put(cities.get("Daugavpils"),1082 );
        directDistancesToNN.put(cities.get("Saint_Petersburg"), 896);
        directDistancesToNN.put(cities.get("Riga"), 1215);
        directDistancesToNN.put(cities.get("Murmansk"), 1508);
        directDistancesToNN.put(cities.get("Odessa"), 1428);
        directDistancesToNN.put(cities.get("Kazan"), 324);
        directDistancesToNN.put(cities.get("Minsk"), 1077);
        directDistancesToNN.put(cities.get("Kaunas"), 1264);
        directDistancesToNN.put(cities.get("Tallinn"),1183 );
        directDistancesToNN.put(cities.get("Simferopol"), 1443);
        directDistancesToNN.put(cities.get("Yaroslavl"), 288);
        directDistancesToNN.put(cities.get("Ufa"), 772);
        directDistancesToNN.put(cities.get("Samara"), 524);
        directDistancesToNN.put(cities.get("Kishinev"), 1467);
        directDistancesToNN.put(cities.get("Nizhny_Novgorod"), 0);
    }

    private void aStar() {
        HashMap<City, Status> isViewedCity = new HashMap<>();
        cities.forEach((k, v) -> isViewedCity.put(v, Status.WHITE));

        Stack<City> cityStack = new Stack<>();
        cityStack.push(fromCity);
        isViewedCity.put(fromCity, Status.GRAY);

        NODE_COUNTER++;

        while (!cityStack.isEmpty()) {
            int minValue = Integer.MAX_VALUE;
            City top = cityStack.pop();
            if (top.equals(destCity)) break;
            isViewedCity.put(top, Status.BLACK);

            NODE_COUNTER++;

            for (Map.Entry<City, Integer> entry : top.getDistanceToNeighbourCities().entrySet()) {
                if (isViewedCity.get(entry.getKey()).equals(Status.WHITE) && entry.getValue() + directDistancesToNN.get(entry.getKey()) < minValue) {
                    minValue = entry.getValue() + directDistancesToNN.get(entry.getKey());
                    edges.push(Map.entry(top, entry.getKey()));
                    cityStack.push(entry.getKey());
                    isViewedCity.put(entry.getKey(), Status.GRAY);
                }
            }
        }

        getRightOrder();
    }

    private void getRightOrder() {
        City req = destCity;
        stringBuilder.append(destCity).append(" <=(");
        while (!edges.isEmpty()) {
            Map.Entry<City, City> top = edges.pop();
            if (top.getValue().equals(req)) {
                stringBuilder.append(top.getKey().getDistanceToNeighbourCities().get(req)).append(")== ");
                sum += req.getDistanceToNeighbourCities().get(top.getKey());
                req = top.getKey();
                if (!req.equals(fromCity)) {
                    stringBuilder.append(req).append(" <=(");
                } else {
                    stringBuilder.append(req);
                    break;
                }
            }
        }
    }

    @Override
    public String showPath() {
        return stringBuilder.toString();
    }

    public int getSum() {
        return sum;
    }

    public int getCounter() {
        return NODE_COUNTER;
    }
}
