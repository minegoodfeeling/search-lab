package method;

import entity.City;

import java.util.*;

public class BreadthFirstSearchMethod implements MethodInterface, ShowPathInterface {
    private final HashMap<String, City> cities;
    private final City fromCity;
    private final City destCity;
    private final StringBuilder stringBuilder;
    private int NODE_COUNTER = 0;

    public BreadthFirstSearchMethod(HashMap<String, City> cities, City fromCity, City destCity) {
        this.cities = cities;
        this.fromCity = fromCity;
        this.destCity = destCity;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public void doTask() {
        ArrayList<City> straight = bfs(fromCity, destCity);

        straight.forEach(k -> stringBuilder.append(k.getNameOfCity()).append(" ===> "));
    }

    @Override
    public String showPath() {
        return stringBuilder.append(destCity).append(". End of route!").toString();
    }

    private ArrayList<City> bfs(City currentCity, City destCity) {
        HashSet<City> isVisitedCity = new HashSet<>();
        ArrayList<City> path = new ArrayList<>();
        Stack<Map.Entry<City, City>> edgesStack = new Stack<>();
        isVisitedCity.add(currentCity);

        Queue<City> queue = new ArrayDeque<>();
        queue.add(currentCity);
        while (!queue.isEmpty()) {
            City top = queue.poll();
            if (top.equals(destCity)) {
                break;
            }
            NODE_COUNTER++;

            cities.get(top.getNameOfCity()).getDistanceToNeighbourCities().forEach((k, v) -> {
                if (!isVisitedCity.contains(k)) {
                    isVisitedCity.add(k);
                    edgesStack.push(Map.entry(top, k));
                    queue.add(k);
                }
            });
        }

        getRightPath(edgesStack, path);

        return path;
    }

    void getRightPath(Stack<Map.Entry<City, City>> edgesStack, ArrayList<City> path) {
        City req = destCity;
        while (!edgesStack.isEmpty()) {
            Map.Entry<City, City> top = edgesStack.pop();

            if (top.getValue().equals(req)) {
                req = top.getKey();
                path.add(req);
            }
        }
        Collections.reverse(path);
    }

    public int getCounter() {
        return NODE_COUNTER;
    }

}
