package entity;

@Deprecated
public class CityWithStatus extends City {

    private Enum<Status> status = Status.WHITE;

    public CityWithStatus(String nameOfCity) {
        super(nameOfCity);
    }

    public Enum<Status> getStatus() {
        return status;
    }

    public void setStatus(Enum<Status> status) {
        this.status = status;
    }

}