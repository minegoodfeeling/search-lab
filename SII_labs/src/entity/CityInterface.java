package entity;

import java.util.HashMap;

public interface CityInterface {

    String getNameOfCity();
    void addDistanceToNeighbourCities(City neighbour, Integer distance);
    HashMap<City, Integer> getDistanceToNeighbourCities();
}
