package entity;

import java.util.HashMap;
import java.util.Objects;

public class City implements CityInterface {

    private final String nameOfCity;

    private final HashMap<City, Integer> distanceToNeighbourCities = new HashMap<>();

    public City(String nameOfCity) {
        this.nameOfCity = nameOfCity;
    }

    @Override
    public String getNameOfCity() {
        return nameOfCity;
    }

    public HashMap<City, Integer> getDistanceToNeighbourCities() {
        return distanceToNeighbourCities;
    }

    @Override
    public void addDistanceToNeighbourCities(City neighbour, Integer distance) {
        distanceToNeighbourCities.put(neighbour, distance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return nameOfCity.equals(city.nameOfCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfCity);
    }

    @Override
    public String toString() {
        return nameOfCity;
    }
}
